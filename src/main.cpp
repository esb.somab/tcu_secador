#include <Arduino.h>
#include <DHT.h>
#include <Keypad.h>
#include <SD.h>
#include "RTClib.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>


LiquidCrystal_I2C lcd(0x27, 16, 2);

//LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
//Constants
#define DHTPIN1 2           // what pin we're connected to
#define DHTPIN2 3           // what pin we're connected to
#define DHTPIN3 4           // what pin we're connected to
#define DHTTYPE DHT22       // DHT 22  (AM2302)
DHT dht1(DHTPIN1, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dht2(DHTPIN2, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dht3(DHTPIN3, DHTTYPE); ////  Initialize DHT sensor for normal 16mhz Arduino
int relay = 5;
const int button = 9;
//comentario initu
const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'*', '0', '#'}};
byte rowPins[ROWS] = {A1, 6, 7, A3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A2, A0, 8};    //connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

File myFile;
const int pinMicro = 10;
int myAVals[3] = {15, 5}; //initiate a 2 element array
DateTime now;

//Variables
int chk;
float hum1;  //Stores humidity value
float temp1; //Stores temperature value
float hum2;  //Stores humidity value
float temp2; //Stores temperature value
float hum3;  //Stores humidity value
float temp3; //Stores temperature value
int estaon;

int posicion = 0; // necesaria para la clave
int cursor = 5;   // posicion inicial de la clave en el LCD
int clave = 0;    // para el LCD
int luz = 0;      // para el LCD
int tiempo = 0;   // para el LCD
// pin para el LED rojo
int buzzer = 10;                              // pin altavoz
char codigoSecreto[4] = {'2', '2', '5', '5'}; // Aqui va el codigo secreto

String stringUser;
String stringTime;
int k;
int l;
//Keypad Teclado1 = Keypad(makeKeymap(Teclas), Pins_Filas, Pins_Cols, Filas, Cols);
void grabar();
void grabar_auxiliar();
void grabar2();
void usuario1();
void usuario2();
void RELAY();
void boton();
void LCD();
void ID();
void MicroSD();
void DS3231();

void setup()
{
  Serial.begin(9600);
  dht1.begin();
  dht2.begin();
  dht3.begin();

  pinMode(relay, OUTPUT);
  pinMode(pinMicro, OUTPUT);
  pinMode(button, INPUT);

  lcd.init();
  lcd.backlight();

  Serial.print("Iniciando SD ...");
  if (!SD.begin(10))
  {
    Serial.println("No se pudo inicializar");
    return;
  }
  Serial.println("inicializacion exitosa");
}

void loop()
{

  //DTH1();
  //Key();
  //DS3231();
  //MicroSD();
  //boton();
  //liquidCristal();
  //RELAY();
  //ID();

  //LCD();
  //usuario1();
  //usuario1();//normla
  //grabar2();

  //grabar_auxiliar();
}

void DTH1()
{
  //Read data and store it to variables hum and temp
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  //Print temp and humidity values to serial monitor
  Serial.print("Humidity#1 : ");
  Serial.print(hum1);
  Serial.print(" %, Temp#1 : ");
  Serial.print(temp1);
  Serial.println(" Celsius");
  delay(3000); //Delay 2 sec.

  Serial.print("Humidity#2 : ");
  Serial.print(hum2);
  Serial.print(" %, Temp#2 : ");
  Serial.print(temp2);
  Serial.println(" Celsius");
  delay(3000); //Delay 2 sec.

  Serial.print("Humidity#3 : ");
  Serial.print(hum3);
  Serial.print(" %, Temp#3 : ");
  Serial.print(temp3);
  Serial.println(" Celsius");
  delay(3000); //Delay 2 sec.

} //final dth

void Key()
{
  char key = keypad.getKey();

  if (key != NO_KEY)
  {
    Serial.println(key);
    stringUser += key;
    Serial.println("Usuario:");
    Serial.println(stringUser);
    lcd.setCursor(0, 0);   // situamos el cursor el la pos 0 de la linea 0.
    lcd.print(stringUser); // imprimimos pulsacion
  }

} // final key

void DS3231()
{
  DateTime now = rtc.now();

  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(" (");
  Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
  Serial.print(") ");
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
} //final ds3231

void MicroSD()
{
  myFile = SD.open("Final3.csv", FILE_WRITE); //abrimos  el archivo
  now = rtc.now();
  stringTime = "";
  stringTime += now.hour();
  stringTime += ":";
  stringTime += now.minute();
  stringTime += ":";
  stringTime += now.second();

  hum1 = dht1.readHumidity();
  // Read temperature as Celsius
  temp1 = dht1.readTemperature();
  if (myFile)
  {
    myFile = SD.open("stringUser.txt", FILE_WRITE);

    Serial.print("Hora: ");
    Serial.print(stringTime);
    Serial.print("  ");
    Serial.print("Humidity: ");
    Serial.print(hum1);
    Serial.print(" %");
    Serial.print("  ");
    Serial.print("Temperature: ");
    Serial.print(temp1);
    Serial.println(" *C");
    Serial.print("  ");
    myFile.print(",");
    myFile.print("Temperatura");
    myFile.print(",");
    myFile.print(temp1);
    myFile.print(",");
    myFile.print("Humedad");
    myFile.print(",");
    myFile.println(hum1);
    Serial.print("Sirvio");
    // close the file:
    myFile.close();
    Serial.println("done.");
  }
  else
  {
    Serial.println("Error al abrir el archivo");
    delay(3000);
  }
  delay(1000);

} //final micro

void boton()
{

  estaon = digitalRead(button);

  // SE OPRIMIO EL BOTON DE ENCENDIDO?
  if (estaon == LOW)
  {
    // ENTONCES ENCENDEMOS EL LED
    digitalWrite(relay, HIGH);
  }

  // SE OPRIMIO EL BOTON DE APAGADO?
  if (estaon == HIGH)
  {
    // ENTONCES APAGAMOS EL LED
    digitalWrite(relay, LOW);
  }

} //final boton

void liquidCristal()
{
  lcd.backlight();
  lcd.setCursor(5, 0);
  lcd.print("Hello ");
  lcd.setCursor(0, 1);
  lcd.print("Valeria");
} //final lcd

void RELAY()
{
  digitalWrite(relay, HIGH);
  delay(500);
  digitalWrite(relay, LOW);
  delay(500);

} //fial relay

void ID()
{
  estaon = digitalRead(button);

  char pulsacion = keypad.getKey(); // leemos pulsacion
  if (pulsacion != 0)
  { // descartamos almohadilla y asterisco
    //Si el valor es 0 es que no se ha pulsado ninguna tecla
    if (pulsacion != '#' && pulsacion != '*' && clave == 0)
    {
      lcd.print(pulsacion); // imprimimos pulsacion
      cursor++;             // incrementamos el cursor

      //--- Condicionales para comprobar la clave introducida -----------
      // comparamos entrada con cada uno de los digitos, uno a uno
      //if (pulsacion == codigoSecreto[posicion])
      posicion++; // aumentamos posicion si es correcto el digito

      if (posicion == 4)
      {                         // comprobamos que se han introducido los 4 correctamente
        digitalWrite(13, HIGH); // encendemos LED
        lcd.setCursor(0, 0);    // situamos el cursor el la pos 0 de la linea 0.
        lcd.print("Clave correcta ");
        lcd.print("Usuario Correcto? ");
        Serial.println("  ");
        Serial.println("Usuario Correcto? "); // escribimos en LCD
        Serial.println("SI(BOTON) NO(*)");    // escribimos en LCD
        delay(200);                           // tono de clave correcta

        lcd.setCursor(2, 1); // cursor en la posicion 5, linea 1
        clave = 1;           // indicamos que se ha introducido la clave

      } //posicion == 4

    } //pulsacion != '#' && pulsacion != '*' && clave==0
  }   //pulsacion != 0

  //--- Condicionales para encender o apagar el LCD --------------
  if (pulsacion == '#' && luz == 0)
  {                  // comprobamos tecla y encendemos si esta apagado
    lcd.backlight(); // encendemos
    luz = 1;         // indicamos que esta encendida
    pulsacion = 0;   // borramos el valor para poder leer el siguiente condicional
  }

  if (pulsacion == '#' && luz == 1)
  {                    // comprobamos tecla y estado
    lcd.noBacklight(); // apagamos
    luz = 0;           // indicamos que esta apagada
  }

  //--- Condicionales para resetear clave introducida -------------
  if (pulsacion == '*')
  { // asterisco para resetear el contador
    posicion = 0;
    cursor = 5;
    clave = 0;
    posicion = 0;
    lcd.setCursor(0, 0);     // situamos el cursor el la posición 2 de la linea 0.
    lcd.print("Introduzca"); // escribimos en LCD
    lcd.setCursor(5, 1);
    lcd.print(" "); // borramos de la pantalla los numeros
    lcd.setCursor(5, 1);
  }
} //final id

void LCD()
{
  // initialize the lcd
  // Print a message to the LCD.

  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #2:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp2);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #2:");
  lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(hum2);
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #3:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp3);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #3:");
  lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(hum3);
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #1:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp1);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #1:");
  lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(hum1);
} //final lcd

void usuario1()
{
  estaon = digitalRead(button);
  char pulsacion = keypad.getKey(); // leemos pulsacion
  myFile = SD.open("eee.txt", FILE_WRITE);

  // if the file opened okay, write to it:

  now = rtc.now();
  stringTime = "";
  stringTime += now.hour();
  stringTime += ":";
  stringTime += now.minute();
  stringTime += ":";
  stringTime += now.second();

  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  delay(1000);

  //Si el valor es 0 es que no se ha pulsado ninguna tecla
  if (pulsacion != 0)
  { // descartamos almohadilla y asterisco
    if (pulsacion != '#' && pulsacion != '*' && clave == 0)
    {
      lcd.clear();
      stringUser += pulsacion;
      Serial.println("Usuario:");
      lcd.setCursor(0, 0); // situamos el cursor el la pos 0 de la linea 0.
      lcd.print("ID: ");   // imprimimos pulsacion
      lcd.setCursor(1, 1);
      Serial.print(stringUser);
      lcd.print(stringUser);
      cursor++; // incrementamos el cursor

      //--- Condicionales para comprobar la clave introducida -----------
      // comparamos entrada con cada uno de los digitos, uno a uno

      posicion++;                   // aumentamos posicion si es correcto el digito
      Serial.println("Posicion: "); // escribimos en LCD
      Serial.println(posicion);     // escribimos en LCD

      if (posicion == 4)
      { // comprobamos que se han introducido los 4 correctamente
        k = 1;
        lcd.clear();

        lcd.setCursor(0, 0);     // situamos el cursor el la pos 0 de la linea 0.
        lcd.print("Correcto?:"); // imprimimos pulsacion
        lcd.setCursor(11, 0);
        lcd.print(stringUser);
        lcd.clear();
        lcd.setCursor(0, 0); // situamos el cursor el la pos 0 de la linea 0.
        lcd.println("NO(*) SI(Boton) ");
        k = 1;
        Serial.println(k); // escribimos en LCD
        delay(200);        // tono de clave correcta
        clave = 1;         // indicamos que se ha introducido la clave

      } //posicion == 4
      //--- En el caso de que este incompleta o no hayamos acertado ----------
      // comprobamos que no pase de la cuarta posicion
      if (cursor > 8)
      {
        cursor = 5;   // lo volvemos a colocar al inicio
        posicion = 0; // borramos clave introducida
        lcd.setCursor(0, 1);
        lcd.print(stringUser); // borramos la clave de la pantalla
        lcd.setCursor(0, 1);
        if (clave == 0) // comprobamos que no hemos acertado
        {
          ;           // para generar
          delay(250); // tono de error
        }
      } //cursor>8
    }   //pulsacion != '#' && pulsacion != '*' && clave==0
  }     //pulsacion != 0

  //--- Condicionales para encender o apagar el LCD --------------
  if (pulsacion == '#' && luz == 0)
  { // comprobamos tecla y encendemos si esta apagado
    // tono de clave correcta

    lcd.backlight(); // encendemos
    luz = 1;         // indicamos que esta encendida
    pulsacion = 0;   // borramos el valor para poder leer el siguiente condicional
  }

  if (pulsacion == '#' && luz == 1)
  {                    // comprobamos tecla y estado
    lcd.noBacklight(); // apagamos
    luz = 0;           // indicamos que esta apagada
  }

  //--- Condicionales para resetear clave introducida -------------
  switch (pulsacion)
  { // asterisco para resetear el contador
  case '*':
    k = 0;
    Serial.println(k); // escribimos en LCD
    delay(200);
    posicion = 0;
    cursor = 5;
    clave = 0;
    posicion = 0;
    lcd.clear();
    stringUser = "";
    break;

  } //switch

  switch (k)
  { // asterisco para resetear el contador
  case 1:
    if (estaon == LOW && k == 1)
    {
      // ENTONCES ENCENDEMOS EL LED
      l = 1;
    }
    if (l == 1)
    {
      // ENTONCES ENCENDEMOS EL LE
      digitalWrite(relay, HIGH);
      int tiempoPantalla1 = 1000;
      int tiempoPantalla2 = 1000;
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("UCR TCU-230");
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Prototipo Secador");

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #1:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp1);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #1:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum1);

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #2:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp2);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #2:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum2);

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #3:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp3);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #3:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum3);

      while (l == 1)
      {

        delay(tiempoPantalla2);
        lcd.noBacklight();
        myFile.print(",");
        myFile.print("Temperatura");
        myFile.print(",");
        myFile.print(temp1);
        myFile.print(",");
        myFile.print("Humedad");
        myFile.print(",");
        myFile.println(hum1);
        myFile.print(",");
        myFile.print("Temperatura");
        myFile.print(",");
        myFile.print(temp2);
        myFile.print(",");
        myFile.print("Humedad");
        myFile.print(",");
        myFile.println(hum2);
        myFile.print(",");
        myFile.print("Temperatura");
        myFile.print(",");
        myFile.print(temp3);
        myFile.print(",");
        myFile.print("Humedad");
        myFile.print(",");
        myFile.println(hum3);
      } //while

      //******************

      k++;
    }

    break;
  case 0:
    if (estaon == LOW && k == 0)
    {
      // ENTONCES ENCENDEMOS EL LED
      l = 0;
    }
    if (l == 0)
    {
      // ENTONCES ENCENDEMOS EL LE
      digitalWrite(relay, LOW);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("SECADO");
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("TERMINADO");
      Serial.println(" Pause "); // escribimos en LCD
      k = 2;
      myFile.close();
      delay(1000);
      lcd.clear();
    }

    break;

  } // switch

} //final usuario

void grabar()
{

  myFile = SD.open("datalog.txt", FILE_WRITE); //abrimos  el archivo

  // if the file opened okay, write to it:
  int tiempoSD = 2000;
  int tiempoSD1 = 2000;
  now = rtc.now();
  stringTime = "";
  stringTime += now.hour();
  stringTime += ":";
  stringTime += now.minute();
  stringTime += ":";
  stringTime += now.second();

  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();

  delay(tiempoSD1);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #2:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp2);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #2:");
  lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(hum2);
  delay(tiempoSD1);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #3:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp3);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #3:");
  lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(hum3);
  delay(tiempoSD1);
  lcd.clear();
  lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Temp #1:");
  lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print(temp1);
  lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
  lcd.print("Hum #1:");
  lcd.setCursor(8, 1); //
  lcd.print(hum1);

  Serial.print("Hora: ");
  Serial.print(stringTime);
  delay(tiempoSD);
  Serial.print("  ");
  Serial.print("Humidity: ");
  Serial.print(hum1);
  Serial.print(" %");
  delay(tiempoSD);
  Serial.print("  ");
  Serial.print("Temperature: ");
  Serial.print(temp1);
  Serial.println(" *C");

  //***********************

  if (myFile)
  {
    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Temperatura1");
    myFile.print(",");
    myFile.print(temp1);

    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Humedad1");
    myFile.print(",");
    myFile.println(hum1);

    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Temperatura2");
    myFile.print(",");
    myFile.print(temp2);

    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Humedad2");
    myFile.print(",");
    myFile.println(hum2);

    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Temperatura3");
    myFile.print(",");
    myFile.print(temp3);

    delay(tiempoSD);
    myFile.print(",");
    myFile.print("Humedad3");
    myFile.print(",");
    myFile.println(hum3);
  }
  else
  {
    Serial.println("Error al abrir el archivo");
  }
  delay(tiempoSD);
  Serial.print("Sirvio");
  // close the file:

  myFile.close();
  Serial.println("done.");

} //final grabar

void usuario2()
{
  estaon = digitalRead(button);
  char pulsacion = keypad.getKey(); // leemos pulsacion
  myFile = SD.open("stringUser", FILE_WRITE);

  // if the file opened okay, write to it:

  now = rtc.now();
  stringTime = "";
  stringTime += now.hour();
  stringTime += ":";
  stringTime += now.minute();
  stringTime += ":";
  stringTime += now.second();

  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  delay(1000);

  //Si el valor es 0 es que no se ha pulsado ninguna tecla
  if (pulsacion != 0)
  { // descartamos almohadilla y asterisco
    if (pulsacion != '#' && pulsacion != '*' && clave == 0)
    {
      lcd.clear();
      stringUser += pulsacion;
      Serial.println("Usuario:");
      lcd.setCursor(0, 0); // situamos el cursor el la pos 0 de la linea 0.
      lcd.print("ID: ");   // imprimimos pulsacion
      lcd.setCursor(1, 1);
      Serial.print(stringUser);
      lcd.print(stringUser);
      cursor++; // incrementamos el cursor

      //--- Condicionales para comprobar la clave introducida -----------
      // comparamos entrada con cada uno de los digitos, uno a uno

      posicion++;                   // aumentamos posicion si es correcto el digito
      Serial.println("Posicion: "); // escribimos en LCD
      Serial.println(posicion);     // escribimos en LCD

      if (posicion == 4)
      { // comprobamos que se han introducido los 4 correctamente
        k = 1;
        lcd.clear();

        lcd.setCursor(0, 0);     // situamos el cursor el la pos 0 de la linea 0.
        lcd.print("Correcto?:"); // imprimimos pulsacion
        lcd.setCursor(11, 0);
        lcd.print(stringUser);
        lcd.clear();
        lcd.setCursor(0, 0); // situamos el cursor el la pos 0 de la linea 0.
        lcd.println("NO(*) SI(Boton) ");
        k = 1;
        Serial.println(k); // escribimos en LCD
        delay(200);        // tono de clave correcta
        clave = 1;         // indicamos que se ha introducido la clave

      } //posicion == 4
      //--- En el caso de que este incompleta o no hayamos acertado ----------
      // comprobamos que no pase de la cuarta posicion
      if (cursor > 8)
      {
        cursor = 5;   // lo volvemos a colocar al inicio
        posicion = 0; // borramos clave introducida
        lcd.setCursor(0, 1);
        lcd.print(stringUser); // borramos la clave de la pantalla
        lcd.setCursor(0, 1);
        if (clave == 0) // comprobamos que no hemos acertado
        {
          ;           // para generar
          delay(250); // tono de error
        }
      } //cursor>8
    }   //pulsacion != '#' && pulsacion != '*' && clave==0
  }     //pulsacion != 0

  //--- Condicionales para encender o apagar el LCD --------------
  if (pulsacion == '#' && luz == 0)
  { // comprobamos tecla y encendemos si esta apagado
    // tono de clave correcta

    lcd.backlight(); // encendemos
    luz = 1;         // indicamos que esta encendida
    pulsacion = 0;   // borramos el valor para poder leer el siguiente condicional
  }

  if (pulsacion == '#' && luz == 1)
  {                    // comprobamos tecla y estado
    lcd.noBacklight(); // apagamos
    luz = 0;           // indicamos que esta apagada
  }

  //--- Condicionales para resetear clave introducida -------------
  switch (pulsacion)
  { // asterisco para resetear el contador
  case '*':
    k = 0;
    Serial.println(k); // escribimos en LCD
    delay(200);
    posicion = 0;
    cursor = 5;
    clave = 0;
    posicion = 0;
    lcd.clear();
    stringUser = "";
    break;

  } //switch

  switch (k)
  { // asterisco para resetear el contador
  case 1:
    if (estaon == LOW && k == 1)
    {
      // ENTONCES ENCENDEMOS EL LED
      l = 1;
    }
    if (l == 1)
    {
      // ENTONCES ENCENDEMOS EL LE
      digitalWrite(relay, HIGH);
      int tiempoPantalla1 = 1000;
      int tiempoPantalla2 = 1000;

      //while(l==1){
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("UCR TCU-230");
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Prototipo Secador");

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #1:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp1);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #1:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum1);

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #2:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp2);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #2:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum2);

      delay(tiempoPantalla1);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Temp #3:");
      lcd.setCursor(9, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(temp3);
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("Hum #3:");
      lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print(hum3);
      //}//while

      delay(tiempoPantalla2);
      lcd.noBacklight();
      myFile.print(",");
      myFile.print("Temperatura");
      myFile.print(",");
      myFile.print(temp1);
      myFile.print(",");
      myFile.print("Humedad");
      myFile.print(",");
      myFile.println(hum1);
      myFile.print(",");
      myFile.print("Temperatura");
      myFile.print(",");
      myFile.print(temp2);
      myFile.print(",");
      myFile.print("Humedad");
      myFile.print(",");
      myFile.println(hum2);
      myFile.print(",");
      myFile.print("Temperatura");
      myFile.print(",");
      myFile.print(temp3);
      myFile.print(",");
      myFile.print("Humedad");
      myFile.print(",");
      myFile.println(hum3);
      //******************

      k++;
    }

    break;
  case 0:
    if (estaon == LOW && k == 0)
    {
      // ENTONCES ENCENDEMOS EL LED
      l = 0;
    }
    if (l == 0)
    {
      // ENTONCES ENCENDEMOS EL LE
      digitalWrite(relay, LOW);
      lcd.clear();
      lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("SECADO");
      lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
      lcd.print("TERMINADO");
      Serial.println(" Pause "); // escribimos en LCD
      k = 2;
      myFile.close();
      delay(1000);
      lcd.clear();
    }

    break;

  } // switch

} //final usuario

void grabar2()
{
  myFile = SD.open("datalog56.txt", FILE_WRITE); //abrimos  el archivo

  if (myFile)
  {
    Serial.print("Escribiendo SD: ");
    int sensor1 = analogRead(0);
    int sensor2 = analogRead(1);
    int sensor3 = analogRead(2);
    myFile.print("Tiempo(ms)=");
    myFile.print(millis());
    myFile.print(", sensor1=");
    myFile.print(sensor1);
    myFile.print(", sensor2=");
    myFile.print(sensor2);
    myFile.print(", sensor3=");
    myFile.println(sensor3);

    myFile.close(); //cerramos el archivo

    Serial.print("Tiempo(ms)=");
    Serial.print(millis());
    Serial.print(", sensor1=");
    Serial.print(sensor1);
    Serial.print(", sensor2=");
    Serial.print(sensor2);
    Serial.print(", sensor3=");
    Serial.println(sensor3);
  }
  else
  {
    Serial.println("Error al abrir el archivo");
  }
  delay(100);
}

void grabar_auxiliar()
{
  digitalWrite(relay, HIGH);
  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  // make a string for assembling the data to log:
  String dataString = "";

  // read three sensors and append to the string:
  for (int analogPin = 0; analogPin < 3; analogPin++)
  {
    int sensor = analogRead(analogPin);
    dataString += String(sensor);
    if (analogPin < 2)
    {
      dataString += ",";
    }
  }

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("SECADOR.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile)
  {
    int tiempoSD = 9000;
    int tiempoPantalla = 10000;
    dataFile.print("Temp #1= ");
    dataFile.print(temp1);
    dataFile.print(", Hum #1 = ");
    dataFile.print(hum1);
    dataFile.print("Temp #2= ");
    dataFile.print(temp2);
    dataFile.print(", Hum #2 = ");
    //dataFile.print(", Hora: ");
    //dataFile.print(StringTime);
    dataFile.print(hum2);
    dataFile.print("Temp #3= ");
    dataFile.print(temp3);
    dataFile.print(", Hum #3 = ");
    dataFile.println(hum3);
    dataFile.close();
    delay(tiempoSD);
    // print to the serial port too:
    Serial.println(dataString);
    delay(tiempoPantalla);
    lcd.clear();
    lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Temp #2:");
    lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(temp2);
    lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Hum #2:");
    lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(hum2);

    delay(tiempoPantalla);
    lcd.clear();
    lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Temp #3:");
    lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(temp3);
    lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Hum #3:");
    lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(hum3);

    delay(tiempoPantalla);
    lcd.clear();
    lcd.setCursor(0, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Temp #1:");
    lcd.setCursor(9, 0); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(temp1);
    lcd.setCursor(0, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print("Hum #1:");
    lcd.setCursor(8, 1); // situamos el cursor el la posiciÃ³n 2 de la linea 0.
    lcd.print(hum1);
  }
  // if the file isn't open, pop up an error:
  else
  {
    Serial.println("error opening datalog.txt");
  }
}